﻿using System;
using Microsoft.EntityFrameworkCore;

namespace ResourceManager.Data.Abstractions
{
    public interface IUnitOfWork : IDisposable
    {
        DbContext Context { get; }

        void SaveChanges();
    }
}
