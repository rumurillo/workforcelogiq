﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace ResourceManager.Data.Abstractions
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IUnitOfWork UnitOfWork { get; }

        IEnumerable<TEntity> Get();

        IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> predicate);

        TEntity Find(Expression<Func<TEntity, bool>> predicate);

        void Add(TEntity entity);
    }
}
