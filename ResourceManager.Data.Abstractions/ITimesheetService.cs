﻿using System.Collections.Generic;

namespace ResourceManager.Data.Abstractions
{
    public interface ITimesheetService<TEntity> where TEntity : class
    {
        /// <summary>
        /// Gets all Timesheets associated to user.
        /// </summary>
        /// <param name="userId">User id to fetch timesheets from.</param>
        /// <returns>List of timesheets by user id.</returns>
        IEnumerable<TEntity> GetTimesheetById(int userId);

        /// <summary>
        /// Inserts a new Timesheet into [Timesheet] table.
        /// </summary>
        /// <param name="entity">New Timesheet to be store.</param>
        /// /// <param name="userId">User id.</param>
        void Add(TEntity entity, int userId);
    }
}
