﻿using System.Collections.Generic;

namespace ResourceManager.Data.Abstractions
{
    public interface IResourceService<TEntity> where TEntity : class
    {
        /// <summary>
        /// Gets all Resources.
        /// </summary>
        /// <returns>Returns list of stored resources.</returns>
        IEnumerable<TEntity> Get();

        /// <summary>
        /// Inserts a new Resource into [Resource] table.
        /// </summary>
        /// <param name="entity">New Resource to be store.</param>
        void Add(TEntity entity);
    }
}
