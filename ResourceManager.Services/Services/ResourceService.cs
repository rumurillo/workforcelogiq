﻿using System;
using System.Collections.Generic;
using ResourceManager.Domain.Models;
using ResourceManager.Data.Abstractions;

namespace ResourceManager.Services.Services
{
    public class ResourceService : IResourceService<Resource>
    {
        private readonly IRepository<Resource> _resourceRepository;
        public ResourceService(IRepository<Resource> resourceRepository)
        {
            _resourceRepository = resourceRepository;
        }

        public void Add(Resource entity)
        {
            try
            {
                _resourceRepository.Add(entity);
                _resourceRepository.UnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                //Log exception...
                throw;
            }
        }

        public IEnumerable<Resource> Get()
        {
            try
            {
                return _resourceRepository.Get();
            }
            catch (Exception ex)
            {
                //Log exception...
                throw;
            }
        }
    }
}
