﻿using System;
using System.Collections.Generic;
using ResourceManager.Domain.Models;
using ResourceManager.Data.Abstractions;

namespace ResourceManager.Services.Services
{
    public class TimesheetService : ITimesheetService<Timesheet>
    {
        private readonly IRepository<Timesheet> _timesheetRepository;
        private readonly IRepository<Resource> _resourceRepository;

        public TimesheetService(IRepository<Timesheet> timesheetRepository, IRepository<Resource> resourceRepository)
        {
            _timesheetRepository = timesheetRepository;
            _resourceRepository = resourceRepository;
        }

        public IEnumerable<Timesheet> GetTimesheetById(int userId)
        {
            try
            {
                return _timesheetRepository.Get(x => x.ResourceId == userId);
            }
            catch (Exception ex)
            {
                //Log exception...
                throw;
            }
        }

        public void Add(Timesheet entity, int userId)
        {
            try
            {
                if (_resourceRepository.Find(resource => resource.Id == userId) == null)
                    throw new Exception($"Unable to find a {nameof(Resource)} with id: {userId}");

                _timesheetRepository.Add(entity);
                _timesheetRepository.UnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                //Log exception...
                throw;
            }
        }
    }
}
