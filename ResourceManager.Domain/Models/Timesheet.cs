﻿using System;
using System.Collections.Generic;

namespace ResourceManager.Domain.Models
{
    public partial class Timesheet
    {
        public int Id { get; set; }
        public DateTime? Date { get; set; }
        public int? HoursWorked { get; set; }
        public int ResourceId { get; set; }

        public Resource Resource { get; set; }
    }
}
