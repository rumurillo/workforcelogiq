﻿using Microsoft.EntityFrameworkCore;

namespace ResourceManager.Domain.Models
{
    public partial class ResourceManagerContext : DbContext
    {
        public ResourceManagerContext()
        {
        }

        public ResourceManagerContext(DbContextOptions<ResourceManagerContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Resource> Resource { get; set; }
        public virtual DbSet<Timesheet> Timesheet { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //Assume we have db connection safely stored in a webconfig/appsettings.json file.
                optionsBuilder.UseSqlServer("Server=.\\SQLExpress;Database=ResourceManager;User ID=jmurilru;pwd=;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Resource>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.FirstName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Timesheet>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Date).HasColumnType("date");

                entity.HasOne(d => d.Resource)
                    .WithMany(p => p.Timesheet)
                    .HasForeignKey(d => d.ResourceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RESOURCE");
            });
        }
    }
}
