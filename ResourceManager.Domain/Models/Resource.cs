﻿using System.Collections.Generic;

namespace ResourceManager.Domain.Models
{
    public partial class Resource
    {
        public Resource()
        {
            Timesheet = new HashSet<Timesheet>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public ICollection<Timesheet> Timesheet { get; set; }
    }
}
