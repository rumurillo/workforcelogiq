# Resources Management App
Resource Management app is a highly-decoupled web-based application that allows Resource Management.

## Considerations

* Highly-decoupled: isolated abstractions allows decoupling of layers.
* Because of the architecture, we could manage to implement Polyglot Persistence, since database, models, and services are highly decoupled, we could switch between different database providers without many changes.
* Repository+UnitOfWork: allows for TDD, maintainability, scalability, and decoupling. (Unable to provide a Test project, since time did not allow it.)
* A database-first EntityFramework approach has been used.

## Assumptions

* We assume we have our database connection safely stored in a webconfig/appsettings.json file.
* We assume we are building a monolithic application, because of the application's nature, we do not require much more complex architectures.
* Assume we are logging exceptions appropriately.
* Assume we have Authorization/Authentication filters applied on webservice.


### Database Scripts

The following, are the database scripts for both tables, Resource and Timesheet.

```
CREATE TABLE [Resource](
	Id INT NOT NULL PRIMARY KEY,
	FirstName VARCHAR(255),
	LastName VARCHAR(255)
)

CREATE TABLE [Timesheet](
	Id INT NOT NULL PRIMARY KEY,
	Date DATE,
	HoursWorked INT,
	ResourceId INT NOT NULL,
	CONSTRAINT FK_RESOURCE FOREIGN KEY (ResourceId) REFERENCES [Resource](Id)
)
```

