﻿using Microsoft.AspNetCore.Mvc;
using ResourceManager.Domain.Models;
using ResourceManager.Data.Abstractions;

namespace ResourceManager.WebService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TimesheetController : ControllerBase
    {
        private readonly ITimesheetService<Timesheet> _timesheetService;

        public TimesheetController(ITimesheetService<Timesheet> timesheetService)
        {
            _timesheetService = timesheetService;
        }

        [HttpGet]
        [Route("Get")]
        public IActionResult GetTimesheets(int userId)
        {
            var result = _timesheetService.GetTimesheetById(userId);

            return Ok(result);
        }

        [HttpPost]
        [Route("Add")]
        public IActionResult AddTimesheet(Timesheet newTimesheet, int userId)
        {
            if (newTimesheet == null)
                return BadRequest($"Please, send in timesheet.");

            _timesheetService.Add(newTimesheet, userId);

            return Ok();
        }
    }
}