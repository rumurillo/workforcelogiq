﻿using Microsoft.AspNetCore.Mvc;
using ResourceManager.Domain.Models;
using ResourceManager.Data.Abstractions;

namespace ResourceManager.WebService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ResourceController : ControllerBase
    {
        private readonly IResourceService<Resource> _resourceService;

        public ResourceController(IResourceService<Resource> resourceService)
        {
            _resourceService = resourceService;
        }

        [HttpGet]
        [Route("Get")]
        public IActionResult GetResources()
        {
            var result = _resourceService.Get();

            return Ok(result);
        }

        [HttpPost]
        [Route("Add")]
        public IActionResult AddResource(Resource newResource)
        {
            if (newResource == null)
                return BadRequest($"Please, send in Resource.");

            _resourceService.Add(newResource);

            return Ok();
        }

        //[HttpPatch]
        //[Route("Update")]
        //public IActionResult UpdateResource()
        //{
        //    return Ok();
        //}
    }
}