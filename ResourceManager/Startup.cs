﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ResourceManager.Data.Abstractions;
using ResourceManager.Domain.Models;
using ResourceManager.Services.Services;
using Microsoft.Extensions.DependencyInjection;
using ResourceManager.DataAccess.EF.DataAccess;

namespace ResourceManager.WebService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDbContext<ResourceManagerContext>(item => item.UseSqlServer(
                Configuration.GetConnectionString("ResourceManagerDbConnection")));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            //Services
            services.AddScoped<IResourceService<Resource>, ResourceService>();
            services.AddScoped<ITimesheetService<Timesheet>, TimesheetService>();

            //Repositories
            services.AddScoped<IRepository<Resource>, Repository<Resource>>();
            services.AddScoped<IRepository<Timesheet>, Repository<Timesheet>>();

            //Setup UnitOfWork
            services.AddScoped<IUnitOfWork, UnitOfWork>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
