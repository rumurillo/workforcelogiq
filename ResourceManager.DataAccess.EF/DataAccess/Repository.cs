﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using ResourceManager.Data.Abstractions;

namespace ResourceManager.DataAccess.EF.DataAccess
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        public IUnitOfWork UnitOfWork { get; }

        public Repository(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        public IEnumerable<TEntity> Get()
        {
            return UnitOfWork.Context.Set<TEntity>().AsEnumerable();
        }

        public IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> predicate)
        {
            return UnitOfWork.Context.Set<TEntity>().Where(predicate).AsEnumerable();
        }

        public TEntity Find(Expression<Func<TEntity, bool>> predicate)
        {
            return UnitOfWork.Context.Set<TEntity>().Where(predicate).First();
        }

        public void Add(TEntity entity)
        {
            UnitOfWork.Context.Set<TEntity>().Add(entity);
        }
    }
}
