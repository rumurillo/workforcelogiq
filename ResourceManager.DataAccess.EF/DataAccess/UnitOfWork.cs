﻿using Microsoft.EntityFrameworkCore;
using ResourceManager.Data.Abstractions;
using ResourceManager.Domain.Models;

namespace ResourceManager.DataAccess.EF.DataAccess
{
    public class UnitOfWork : IUnitOfWork
    {
        public DbContext Context { get; }

        public UnitOfWork(ResourceManagerContext context)
        {
            Context = context;
        }

        public void SaveChanges()
        {
            Context.SaveChanges();
        }

        public void Dispose()
        {
            Context.Dispose();
        }
    }
}
