import React, { Component } from 'react';

export class FetchData extends Component {
  displayName = FetchData.name

  constructor(props) {
    super(props);
    this.state = { resources: [], loading: true };

    fetch('api/SampleData/WeatherForecasts')
      .then(response => response.json())
      .then(data => {
        console.log(data);
        this.setState({ resources: data, loading: false });
      });

      fetch('https://localhost:44322/api/resource/get')
      .then(response => response.json())
      .then(data =>{
        console.log(data);
        this.setState({ resources: data, loading: false });
      });
  }

  static renderResourcesTable(resources) {
    return (
      <table className='table'>
        <thead>
          <tr>
            <th>Id</th>
            <th>First Name</th>
            <th>Last Name</th>
          </tr>
        </thead>
        <tbody>
          {resources.map(resources =>
            <tr key={resources.id}>
              <td>{resources.firstname}</td>
              <td>{resources.lastname}</td>
            </tr>
          )}
        </tbody>
      </table>
    );
  }

  render() {
    let contents = this.state.loading
      ? <p><em>Loading...</em></p>
      : FetchData.renderResourcesTable(this.state.resources);

    return (
      <div>
        <h1>Resources</h1>
        <p>The following, are all the active resources for our company.</p>
        {contents}
      </div>
    );
  }
}
